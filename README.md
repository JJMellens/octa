# TACOS (Tracking Altered Code in Obfuscated Software)

## Overview

TACOS is a tool designed to identify common subgraphs in two versions of an application. 
It uses the custom SALSA (Subgraph Approximation using Label and Structure Analysis) algorithm to perform fuzzy matching on nodes based on various heuristics.


### Features

- **Ease of Use**: Simple command-line structure and integrated call-graph generation
- **Androguard cg wrapper**: Includes a simple wrapper for androguard cg to optimize workflow
- **Optimized for Call Graphs**: SALSA algorithm makes use of attributes mapped by androguard cg to speed up subgraph matching
- **High speed**: Precalculated lookup tables and wave-based clique expansion enable sub-minute processing times of entire apk files


#### Androguard Wrapper

TACOS includes a simple wrapper for [androguard cg](https://androguard.readthedocs.io/en/latest/tools/androcg.html) that can be called to automatically generate call graphs from target apk files.
This wrapper does not pass any filtering options and is included only for ease of use.

## Installation

Clone the repository using:
```bash
git clone https://gitlab.com/JJMellens/tacos.git
```

Navigate to the project directory:
```bash
cd tacos
```

Install requirements:
```bash
pip install -r requirements.txt
```

## Usage

### Running TACOS

To run TACOS, place your source files in the `data`  folder and use the following command:
```bash
python main.py gml_file_1 gml_file_2
```

### Options
`tacos.py` accepts various options to customize its behavior. The most important of these are `--alpha` and `--top_hub_percent`
Note that none of the file specifications require an extension as they are striped in the code. 

#### Positional Arguments

- `gml_1`
  - **Description:** Name of the `.gml` file containing graph g1.
  - **Usage:** `python tacos.py gml_1 gml_2`

- `gml_2`
  - **Description:** Name of the `.gml` file containing graph g2.
  - **Usage:** `python tacos.py gml_1 gml_2`

#### Optional Arguments

- `-h`, `--help`
  - **Description:** Show the help message and exit.
  - **Usage:** `python tacos.py --help`

- `-m`, `--module_product_graph`
  - **Description:** Name of the `.gml` file containing the modular product graph of the provided graphs. Skips modular graph creation.
  - **Usage:** `python tacos.py gml_1 gml_2 --module_product_graph mpg.pickle`

- `-l`, `--line_graph`
  - **Description:** Turn g1 and g2 into line graphs first. Significantly increases memory usage for modular graph creation but speeds up maximum clique finding.
  - **Usage:** `python tacos.py gml_1 gml_2 --line_graph`

- `-c`, `--max_clique`
  - **Description:** Name of file containing the maximum clique for the modular product graph of g1 and g2. Skips calculation of the mpg and maximum clique.
  - **Usage:** `python tacos.py gml_1 gml_2 --max_clique clique.pickle`

- `-s`, `--strip_leaves`
  - **Description:** Remove leaf nodes from g1 and g2 to speed up calculations.
  - **Usage:** `python tacos.py gml_1 gml_2 --strip_leaves`

- `-w`, `--write_out`
  - **Description:** Write out intermediate results to protect against data loss in the event of a crash.
  - **Usage:** `python tacos.py gml_1 gml_2 --write_out`

- `-A`, `--algorithm`
  - **Description:** Select clique finding algorithm. Choose between the following:
    - `apx`: approximate networkx algorithm
    - `salsa`: Custom SALSA algorithm (default)
  - **Default:** `salsa`
  - **Usage:** `python tacos.py gml_1 gml_2 --algorithm salsa`

- `-d`, `--debug`
  - **Description:** Set debug level.Choose between the following: 
    - `0`: error
    - `1`: info
    - `2`: timing
    - `3`: debug
    - `4`: super debug
  - **Default:** `info`
  - **Usage:** `python tacos.py gml_1 gml_2 --debug 2`

- `-a`, `--alpha`
  - **Description:** Maximum collision chance when using SALSA. Default is 0.01.
  - **Default:** `0.01`
  - **Usage:** `python tacos.py gml_1 gml_2 --alpha 0.005`

- `t`, `--top_hub_percent`
  - **Description:** Top percentage of hub nodes to check for matching pairs.Higher values increase matches found at the cost of speed. Set between 0 and 1.
  - **Default:** `0.2`
  - **Usage:** `python tacos.py gml_1 gml_2 --top_hub_percent 0.4`
  
- `-g`, `--generate_call_graph`
  - **Description:** Indicate call graphs must be generated first. Assumes gml_1 and gml_2 are .apk files instead.
  - **Usage:** `python .\tacos.py gml_1 gml_2 --generate_call_graph`
  
- `-M`, `--max_taco`
  - **Description:** Enable maximum taco.
  - **Usage:** `python .\tacos.py gml_1 gml_2 --max_taco`


### Note:

Be careful when using the `--module_product_graph` and `--max_clique` options and only use source files you created yourself.
TACOS uses `pickle` to store & read intermediates, which can be a powerful vector for arbitrary code execution.

### Examples

```bash
python tacos.py example1 example2
```
This simplest and recommended form uses the custom SALSA algorithm.
It attempts to generate matching pairs and a shared subgraph from `data/example1.gml` and `data/example2.gml`.


```bash
python main.py example1 example2 -s -w -l -A apx
```
This uses networkx's approximate algorithm and attemps to speed it up by stripping the leaf nodes and using the line graph instead of the normal graph.
It also writes out intermediate files to the `intermediates` folder.


```bash
python main.py example1 example2 -g -a 0.5
```
This uses the default SALSA algorithm and sets the alpha value to 0.5.
Before processing the call graphs it first attempts to generate unfiltered call graphs from `data/example1.apk` and `data/example2.apk`.

## Contributing

Feel free to submit issues and pull requests. For major changes, please open an issue first to discuss what you would like to change.

## License

This project is licensed under the GNU General Public License (GPL).

