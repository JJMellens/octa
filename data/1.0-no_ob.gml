graph [
  directed 1
  node [
    id 0
    label "Lcom/darkempire78/opencalculator/BuildConfig;-><init>()V [access_flags=public constructor] @ 0x24ab60"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "public constructor"
    classname "Lcom/darkempire78/opencalculator/BuildConfig;"
  ]
  node [
    id 1
    label "Ljava/lang/Object;-><init>()V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags ""
    classname "Ljava/lang/Object;"
  ]
  node [
    id 2
    label "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda0;-><init>(Lcom/darkempire78/opencalculator/MainActivity;)V [access_flags=public synthetic constructor] @ 0x24ab78"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity;)V"
    accessflags "public synthetic constructor"
    classname "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda0;"
  ]
  node [
    id 3
    label "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda0;->onClick(Landroid/content/DialogInterface; I)V [access_flags=public final] @ 0x24ab94"
    external 0
    entrypoint 0
    methodname "onClick"
    descriptor "(Landroid/content/DialogInterface; I)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda0;"
  ]
  node [
    id 4
    label "Lcom/darkempire78/opencalculator/MainActivity;->$r8$lambda$rGnxLyjERIOLz5ChMeliIEMFOEc(Lcom/darkempire78/opencalculator/MainActivity; Landroid/content/DialogInterface; I)V [access_flags=public static synthetic] @ 0x24adec"
    external 0
    entrypoint 1
    methodname "$r8$lambda$rGnxLyjERIOLz5ChMeliIEMFOEc"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity; Landroid/content/DialogInterface; I)V"
    accessflags "public static synthetic"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 5
    label "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda1;-><init>(Lcom/darkempire78/opencalculator/MainActivity;)V [access_flags=public synthetic constructor] @ 0x24abd0"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity;)V"
    accessflags "public synthetic constructor"
    classname "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda1;"
  ]
  node [
    id 6
    label "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda1;->onLongClick(Landroid/view/View;)Z [access_flags=public final] @ 0x24abb0"
    external 0
    entrypoint 0
    methodname "onLongClick"
    descriptor "(Landroid/view/View;)Z"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity$$ExternalSyntheticLambda1;"
  ]
  node [
    id 7
    label "Lcom/darkempire78/opencalculator/MainActivity;->$r8$lambda$7OoJHyUOMv2ExK1xVF0a4b4x17M(Lcom/darkempire78/opencalculator/MainActivity; Landroid/view/View;)Z [access_flags=public static synthetic] @ 0x24ad88"
    external 0
    entrypoint 1
    methodname "$r8$lambda$7OoJHyUOMv2ExK1xVF0a4b4x17M"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity; Landroid/view/View;)Z"
    accessflags "public static synthetic"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 8
    label "Lcom/darkempire78/opencalculator/MyPreferences$Companion;-><init>()V [access_flags=private constructor] @ 0x24c078"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/MyPreferences$Companion;"
  ]
  node [
    id 9
    label "Lcom/darkempire78/opencalculator/MyPreferences$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V [access_flags=public synthetic constructor] @ 0x24c090"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Lkotlin/jvm/internal/DefaultConstructorMarker;)V"
    accessflags "public synthetic constructor"
    classname "Lcom/darkempire78/opencalculator/MyPreferences$Companion;"
  ]
  node [
    id 10
    label "Lcom/darkempire78/opencalculator/MyPreferences;-><clinit>()V [access_flags=static constructor] @ 0x24c0c0"
    external 0
    entrypoint 0
    methodname "<clinit>"
    descriptor "()V"
    accessflags "static constructor"
    classname "Lcom/darkempire78/opencalculator/MyPreferences;"
  ]
  node [
    id 11
    label "Lcom/darkempire78/opencalculator/MyPreferences;-><init>(Landroid/content/Context;)V [access_flags=public constructor] @ 0x24c0e4"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Landroid/content/Context;)V"
    accessflags "public constructor"
    classname "Lcom/darkempire78/opencalculator/MyPreferences;"
  ]
  node [
    id 12
    label "Landroid/content/SharedPreferences;->getInt(Ljava/lang/String; I)I"
    external 1
    entrypoint 0
    methodname "getInt"
    descriptor "(Ljava/lang/String; I)I"
    accessflags ""
    classname "Landroid/content/SharedPreferences;"
  ]
  node [
    id 13
    label "Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences; [access_flags=public static] @ 0x20d038"
    external 0
    entrypoint 0
    methodname "getDefaultSharedPreferences"
    descriptor "(Landroid/content/Context;)Landroid/content/SharedPreferences;"
    accessflags "public static"
    classname "Landroidx/preference/PreferenceManager;"
  ]
  node [
    id 14
    label "Lcom/darkempire78/opencalculator/MyPreferences;->getDarkMode()I [access_flags=public final] @ 0x24c0a8"
    external 0
    entrypoint 0
    methodname "getDarkMode"
    descriptor "()I"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MyPreferences;"
  ]
  node [
    id 15
    label "Lcom/darkempire78/opencalculator/MyPreferences;->setDarkMode(I)V [access_flags=public final] @ 0x24c11c"
    external 0
    entrypoint 0
    methodname "setDarkMode"
    descriptor "(I)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MyPreferences;"
  ]
  node [
    id 16
    label "Landroid/content/SharedPreferences$Editor;->apply()V"
    external 1
    entrypoint 0
    methodname "apply"
    descriptor "()V"
    accessflags ""
    classname "Landroid/content/SharedPreferences$Editor;"
  ]
  node [
    id 17
    label "Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String; I)Landroid/content/SharedPreferences$Editor;"
    external 1
    entrypoint 0
    methodname "putInt"
    descriptor "(Ljava/lang/String; I)Landroid/content/SharedPreferences$Editor;"
    accessflags ""
    classname "Landroid/content/SharedPreferences$Editor;"
  ]
  node [
    id 18
    label "Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;"
    external 1
    entrypoint 0
    methodname "edit"
    descriptor "()Landroid/content/SharedPreferences$Editor;"
    accessflags ""
    classname "Landroid/content/SharedPreferences;"
  ]
  node [
    id 19
    label "Lcom/darkempire78/opencalculator/R$anim;-><init>()V [access_flags=private constructor] @ 0x24c164"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$anim;"
  ]
  node [
    id 20
    label "Lcom/darkempire78/opencalculator/R$animator;-><init>()V [access_flags=private constructor] @ 0x24c14c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$animator;"
  ]
  node [
    id 21
    label "Lcom/darkempire78/opencalculator/R$attr;-><init>()V [access_flags=private constructor] @ 0x24c17c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$attr;"
  ]
  node [
    id 22
    label "Lcom/darkempire78/opencalculator/R$bool;-><init>()V [access_flags=private constructor] @ 0x24c194"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$bool;"
  ]
  node [
    id 23
    label "Lcom/darkempire78/opencalculator/R$color;-><init>()V [access_flags=private constructor] @ 0x24c1ac"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$color;"
  ]
  node [
    id 24
    label "Lcom/darkempire78/opencalculator/R$dimen;-><init>()V [access_flags=private constructor] @ 0x24c1c4"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$dimen;"
  ]
  node [
    id 25
    label "Lcom/darkempire78/opencalculator/R$drawable;-><init>()V [access_flags=private constructor] @ 0x24c1dc"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$drawable;"
  ]
  node [
    id 26
    label "Lcom/darkempire78/opencalculator/R$id;-><init>()V [access_flags=private constructor] @ 0x24c1f4"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$id;"
  ]
  node [
    id 27
    label "Lcom/darkempire78/opencalculator/R$integer;-><init>()V [access_flags=private constructor] @ 0x24c20c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$integer;"
  ]
  node [
    id 28
    label "Lcom/darkempire78/opencalculator/R$interpolator;-><init>()V [access_flags=private constructor] @ 0x24c224"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$interpolator;"
  ]
  node [
    id 29
    label "Lcom/darkempire78/opencalculator/R$layout;-><init>()V [access_flags=private constructor] @ 0x24c23c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$layout;"
  ]
  node [
    id 30
    label "Lcom/darkempire78/opencalculator/R$menu;-><init>()V [access_flags=private constructor] @ 0x24c254"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$menu;"
  ]
  node [
    id 31
    label "Lcom/darkempire78/opencalculator/R$mipmap;-><init>()V [access_flags=private constructor] @ 0x24c26c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$mipmap;"
  ]
  node [
    id 32
    label "Lcom/darkempire78/opencalculator/R$string;-><init>()V [access_flags=private constructor] @ 0x24c284"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$string;"
  ]
  node [
    id 33
    label "Lcom/darkempire78/opencalculator/R$style;-><init>()V [access_flags=private constructor] @ 0x24dc04"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$style;"
  ]
  node [
    id 34
    label "Lcom/darkempire78/opencalculator/R$styleable;-><clinit>()V [access_flags=public static constructor] @ 0x24c29c"
    external 0
    entrypoint 0
    methodname "<clinit>"
    descriptor "()V"
    accessflags "public static constructor"
    classname "Lcom/darkempire78/opencalculator/R$styleable;"
  ]
  node [
    id 35
    label "Lcom/darkempire78/opencalculator/R$styleable;-><init>()V [access_flags=private constructor] @ 0x24dbec"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R$styleable;"
  ]
  node [
    id 36
    label "Lcom/darkempire78/opencalculator/R;-><init>()V [access_flags=private constructor] @ 0x24dc1c"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "private constructor"
    classname "Lcom/darkempire78/opencalculator/R;"
  ]
  node [
    id 37
    label "Lcom/darkempire78/opencalculator/MainActivity;->onCreate$lambda-0(Lcom/darkempire78/opencalculator/MainActivity; Landroid/view/View;)Z [access_flags=private static final] @ 0x24ada4"
    external 0
    entrypoint 1
    methodname "onCreate$lambda-0"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity; Landroid/view/View;)Z"
    accessflags "private static final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 38
    label "Lcom/darkempire78/opencalculator/MainActivity;->selectThemeDialog$lambda-1(Lcom/darkempire78/opencalculator/MainActivity; Landroid/content/DialogInterface; I)V [access_flags=private static final] @ 0x24bab4"
    external 0
    entrypoint 1
    methodname "selectThemeDialog$lambda-1"
    descriptor "(Lcom/darkempire78/opencalculator/MainActivity; Landroid/content/DialogInterface; I)V"
    accessflags "private static final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 39
    label "Lcom/darkempire78/opencalculator/MainActivity;-><init>()V [access_flags=public constructor] @ 0x24ae04"
    external 0
    entrypoint 1
    methodname "<init>"
    descriptor "()V"
    accessflags "public constructor"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 40
    label "Landroidx/appcompat/app/AppCompatActivity;-><init>()V [access_flags=public constructor] @ 0x108b78"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags "public constructor"
    classname "Landroidx/appcompat/app/AppCompatActivity;"
  ]
  node [
    id 41
    label "Ljava/util/LinkedHashMap;-><init>()V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags ""
    classname "Ljava/util/LinkedHashMap;"
  ]
  node [
    id 42
    label "Lcom/darkempire78/opencalculator/MainActivity;->checkTheme()V [access_flags=private final] @ 0x24af40"
    external 0
    entrypoint 1
    methodname "checkTheme"
    descriptor "()V"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 43
    label "Lcom/darkempire78/opencalculator/MainActivity;->getDelegate()Landroidx/appcompat/app/AppCompatDelegate;"
    external 1
    entrypoint 1
    methodname "getDelegate"
    descriptor "()Landroidx/appcompat/app/AppCompatDelegate;"
    accessflags ""
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 44
    label "Landroidx/appcompat/app/AppCompatDelegate;->applyDayNight()Z [access_flags=public abstract] @ 0x0"
    external 0
    entrypoint 0
    methodname "applyDayNight"
    descriptor "()Z"
    accessflags "public abstract"
    classname "Landroidx/appcompat/app/AppCompatDelegate;"
  ]
  node [
    id 45
    label "Landroidx/appcompat/app/AppCompatDelegate;->setDefaultNightMode(I)V [access_flags=public static] @ 0x10d7f8"
    external 0
    entrypoint 0
    methodname "setDefaultNightMode"
    descriptor "(I)V"
    accessflags "public static"
    classname "Landroidx/appcompat/app/AppCompatDelegate;"
  ]
  node [
    id 46
    label "Lcom/darkempire78/opencalculator/MainActivity;->getDegreeButton()Landroid/widget/Button; [access_flags=private final] @ 0x24ac38"
    external 0
    entrypoint 1
    methodname "getDegreeButton"
    descriptor "()Landroid/widget/Button;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 47
    label "Lcom/darkempire78/opencalculator/MainActivity;->findViewById(I)Landroid/view/View;"
    external 1
    entrypoint 1
    methodname "findViewById"
    descriptor "(I)Landroid/view/View;"
    accessflags ""
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 48
    label "Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object; Ljava/lang/String;)V [access_flags=public static] @ 0x2b848c"
    external 0
    entrypoint 0
    methodname "checkNotNullExpressionValue"
    descriptor "(Ljava/lang/Object; Ljava/lang/String;)V"
    accessflags "public static"
    classname "Lkotlin/jvm/internal/Intrinsics;"
  ]
  node [
    id 49
    label "Lcom/darkempire78/opencalculator/MainActivity;->getDegreeTextView()Landroid/widget/TextView; [access_flags=private final] @ 0x24ad28"
    external 0
    entrypoint 1
    methodname "getDegreeTextView"
    descriptor "()Landroid/widget/TextView;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 50
    label "Lcom/darkempire78/opencalculator/MainActivity;->getDisplay()Landroid/widget/EditText; [access_flags=private final] @ 0x24ac68"
    external 0
    entrypoint 1
    methodname "getDisplay"
    descriptor "()Landroid/widget/EditText;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 51
    label "Lcom/darkempire78/opencalculator/MainActivity;->getResultDisplay()Landroid/widget/TextView; [access_flags=private final] @ 0x24ad58"
    external 0
    entrypoint 1
    methodname "getResultDisplay"
    descriptor "()Landroid/widget/TextView;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 52
    label "Lcom/darkempire78/opencalculator/MainActivity;->getScientistModeRow2()Landroid/widget/TableRow; [access_flags=private final] @ 0x24acc8"
    external 0
    entrypoint 1
    methodname "getScientistModeRow2"
    descriptor "()Landroid/widget/TableRow;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 53
    label "Lcom/darkempire78/opencalculator/MainActivity;->getScientistModeRow3()Landroid/widget/TableRow; [access_flags=private final] @ 0x24acf8"
    external 0
    entrypoint 1
    methodname "getScientistModeRow3"
    descriptor "()Landroid/widget/TableRow;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 54
    label "Lcom/darkempire78/opencalculator/MainActivity;->getScientistModeSwitchButton()Landroid/widget/ImageButton; [access_flags=private final] @ 0x24ac98"
    external 0
    entrypoint 1
    methodname "getScientistModeSwitchButton"
    descriptor "()Landroid/widget/ImageButton;"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 55
    label "Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V"
    external 1
    entrypoint 0
    methodname "setText"
    descriptor "(Ljava/lang/CharSequence;)V"
    accessflags ""
    classname "Landroid/widget/EditText;"
  ]
  node [
    id 56
    label "Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V"
    external 1
    entrypoint 0
    methodname "setText"
    descriptor "(Ljava/lang/CharSequence;)V"
    accessflags ""
    classname "Landroid/widget/TextView;"
  ]
  node [
    id 57
    label "Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object; Ljava/lang/String;)V [access_flags=public static] @ 0x2b84e0"
    external 0
    entrypoint 0
    methodname "checkNotNullParameter"
    descriptor "(Ljava/lang/Object; Ljava/lang/String;)V"
    accessflags "public static"
    classname "Lkotlin/jvm/internal/Intrinsics;"
  ]
  node [
    id 58
    label "Landroid/content/DialogInterface;->dismiss()V"
    external 1
    entrypoint 0
    methodname "dismiss"
    descriptor "()V"
    accessflags ""
    classname "Landroid/content/DialogInterface;"
  ]
  node [
    id 59
    label "Lcom/darkempire78/opencalculator/MainActivity;->updateResultDisplay()V [access_flags=private final] @ 0x24be0c"
    external 0
    entrypoint 1
    methodname "updateResultDisplay"
    descriptor "()V"
    accessflags "private final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 60
    label "Ljava/lang/String;->charAt(I)C"
    external 1
    entrypoint 0
    methodname "charAt"
    descriptor "(I)C"
    accessflags ""
    classname "Ljava/lang/String;"
  ]
  node [
    id 61
    label "Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object; Ljava/lang/Object;)Z [access_flags=public static] @ 0x2b80a8"
    external 0
    entrypoint 0
    methodname "areEqual"
    descriptor "(Ljava/lang/Object; Ljava/lang/Object;)Z"
    accessflags "public static"
    classname "Lkotlin/jvm/internal/Intrinsics;"
  ]
  node [
    id 62
    label "Ljava/util/Arrays;->copyOf([Ljava/lang/Object; I)[Ljava/lang/Object;"
    external 1
    entrypoint 0
    methodname "copyOf"
    descriptor "([Ljava/lang/Object; I)[Ljava/lang/Object;"
    accessflags ""
    classname "Ljava/util/Arrays;"
  ]
  node [
    id 63
    label "Lorg/mariuszgromada/math/mxparser/Expression;-><init>(Ljava/lang/String; [Lorg/mariuszgromada/math/mxparser/PrimitiveElement;)V [access_flags=public varargs constructor] @ 0x3316ec"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Ljava/lang/String; [Lorg/mariuszgromada/math/mxparser/PrimitiveElement;)V"
    accessflags "public varargs constructor"
    classname "Lorg/mariuszgromada/math/mxparser/Expression;"
  ]
  node [
    id 64
    label "Ljava/lang/String;->valueOf(D)Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "valueOf"
    descriptor "(D)Ljava/lang/String;"
    accessflags ""
    classname "Ljava/lang/String;"
  ]
  node [
    id 65
    label "Lorg/mariuszgromada/math/mxparser/Expression;->calculate()D [access_flags=public] @ 0x32ed70"
    external 0
    entrypoint 0
    methodname "calculate"
    descriptor "()D"
    accessflags "public"
    classname "Lorg/mariuszgromada/math/mxparser/Expression;"
  ]
  node [
    id 66
    label "Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String; C C Z I Ljava/lang/Object;)Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "replace$default"
    descriptor "(Ljava/lang/String; C C Z I Ljava/lang/Object;)Ljava/lang/String;"
    accessflags ""
    classname "Lkotlin/text/StringsKt;"
  ]
  node [
    id 67
    label "Ljava/lang/String;->length()I"
    external 1
    entrypoint 0
    methodname "length"
    descriptor "()I"
    accessflags ""
    classname "Ljava/lang/String;"
  ]
  node [
    id 68
    label "Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;"
    external 1
    entrypoint 0
    methodname "append"
    descriptor "(Ljava/lang/String;)Ljava/lang/StringBuilder;"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 69
    label "Landroid/widget/EditText;->getText()Landroid/text/Editable;"
    external 1
    entrypoint 0
    methodname "getText"
    descriptor "()Landroid/text/Editable;"
    accessflags ""
    classname "Landroid/widget/EditText;"
  ]
  node [
    id 70
    label "Ljava/lang/StringBuilder;-><init>()V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 71
    label "Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;"
    external 1
    entrypoint 0
    methodname "append"
    descriptor "(C)Ljava/lang/StringBuilder;"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 72
    label "Ljava/lang/Object;->toString()Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "toString"
    descriptor "()Ljava/lang/String;"
    accessflags ""
    classname "Ljava/lang/Object;"
  ]
  node [
    id 73
    label "Ljava/lang/String;->format(Ljava/lang/String; [Ljava/lang/Object;)Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "format"
    descriptor "(Ljava/lang/String; [Ljava/lang/Object;)Ljava/lang/String;"
    accessflags ""
    classname "Ljava/lang/String;"
  ]
  node [
    id 74
    label "Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;"
    external 1
    entrypoint 0
    methodname "valueOf"
    descriptor "(D)Ljava/lang/Double;"
    accessflags ""
    classname "Ljava/lang/Double;"
  ]
  node [
    id 75
    label "Ljava/lang/StringBuilder;->toString()Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "toString"
    descriptor "()Ljava/lang/String;"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 76
    label "Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String; Ljava/lang/String; Ljava/lang/String; Z I Ljava/lang/Object;)Ljava/lang/String;"
    external 1
    entrypoint 0
    methodname "replace$default"
    descriptor "(Ljava/lang/String; Ljava/lang/String; Ljava/lang/String; Z I Ljava/lang/Object;)Ljava/lang/String;"
    accessflags ""
    classname "Lkotlin/text/StringsKt;"
  ]
  node [
    id 77
    label "Lcom/darkempire78/opencalculator/MainActivity;->_$_clearFindViewByIdCache()V [access_flags=public] @ 0x24ae30"
    external 0
    entrypoint 1
    methodname "_$_clearFindViewByIdCache"
    descriptor "()V"
    accessflags "public"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 78
    label "Ljava/util/Map;->clear()V"
    external 1
    entrypoint 0
    methodname "clear"
    descriptor "()V"
    accessflags ""
    classname "Ljava/util/Map;"
  ]
  node [
    id 79
    label "Lcom/darkempire78/opencalculator/MainActivity;->_$_findCachedViewById(I)Landroid/view/View; [access_flags=public] @ 0x24abec"
    external 0
    entrypoint 1
    methodname "_$_findCachedViewById"
    descriptor "(I)Landroid/view/View;"
    accessflags "public"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 80
    label "Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;"
    external 1
    entrypoint 0
    methodname "get"
    descriptor "(Ljava/lang/Object;)Ljava/lang/Object;"
    accessflags ""
    classname "Ljava/util/Map;"
  ]
  node [
    id 81
    label "Ljava/util/Map;->put(Ljava/lang/Object; Ljava/lang/Object;)Ljava/lang/Object;"
    external 1
    entrypoint 0
    methodname "put"
    descriptor "(Ljava/lang/Object; Ljava/lang/Object;)Ljava/lang/Object;"
    accessflags ""
    classname "Ljava/util/Map;"
  ]
  node [
    id 82
    label "Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;"
    external 1
    entrypoint 0
    methodname "valueOf"
    descriptor "(I)Ljava/lang/Integer;"
    accessflags ""
    classname "Ljava/lang/Integer;"
  ]
  node [
    id 83
    label "Lcom/darkempire78/opencalculator/MainActivity;->addButton(Landroid/view/View;)V [access_flags=public final] @ 0x24ae4c"
    external 0
    entrypoint 1
    methodname "addButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 84
    label "Lcom/darkempire78/opencalculator/MainActivity;->updateDisplay(Ljava/lang/String;)V [access_flags=public final] @ 0x24bd44"
    external 0
    entrypoint 1
    methodname "updateDisplay"
    descriptor "(Ljava/lang/String;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 85
    label "Lcom/darkempire78/opencalculator/MainActivity;->backspaceButton(Landroid/view/View;)V [access_flags=public final] @ 0x24ae74"
    external 0
    entrypoint 1
    methodname "backspaceButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 86
    label "Landroid/text/Editable;->subSequence(I I)Ljava/lang/CharSequence;"
    external 1
    entrypoint 0
    methodname "subSequence"
    descriptor "(I I)Ljava/lang/CharSequence;"
    accessflags ""
    classname "Landroid/text/Editable;"
  ]
  node [
    id 87
    label "Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;"
    external 1
    entrypoint 0
    methodname "append"
    descriptor "(Ljava/lang/Object;)Ljava/lang/StringBuilder;"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 88
    label "Landroid/text/Editable;->length()I"
    external 1
    entrypoint 0
    methodname "length"
    descriptor "()I"
    accessflags ""
    classname "Landroid/text/Editable;"
  ]
  node [
    id 89
    label "Landroid/widget/EditText;->getSelectionStart()I"
    external 1
    entrypoint 0
    methodname "getSelectionStart"
    descriptor "()I"
    accessflags ""
    classname "Landroid/widget/EditText;"
  ]
  node [
    id 90
    label "Landroid/widget/EditText;->setSelection(I)V"
    external 1
    entrypoint 0
    methodname "setSelection"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/widget/EditText;"
  ]
  node [
    id 91
    label "Lcom/darkempire78/opencalculator/MainActivity;->clearButton(Landroid/view/View;)V [access_flags=public final] @ 0x24afa4"
    external 0
    entrypoint 1
    methodname "clearButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 92
    label "Lcom/darkempire78/opencalculator/MainActivity;->cosinusButton(Landroid/view/View;)V [access_flags=public final] @ 0x24afec"
    external 0
    entrypoint 1
    methodname "cosinusButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 93
    label "Lcom/darkempire78/opencalculator/MainActivity;->degreeButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b028"
    external 0
    entrypoint 1
    methodname "degreeButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 94
    label "Landroid/widget/Button;->getText()Ljava/lang/CharSequence;"
    external 1
    entrypoint 0
    methodname "getText"
    descriptor "()Ljava/lang/CharSequence;"
    accessflags ""
    classname "Landroid/widget/Button;"
  ]
  node [
    id 95
    label "Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V"
    external 1
    entrypoint 0
    methodname "setText"
    descriptor "(Ljava/lang/CharSequence;)V"
    accessflags ""
    classname "Landroid/widget/Button;"
  ]
  node [
    id 96
    label "Lorg/mariuszgromada/math/mxparser/mXparser;->setRadiansMode()V [access_flags=public static] @ 0x342110"
    external 0
    entrypoint 0
    methodname "setRadiansMode"
    descriptor "()V"
    accessflags "public static"
    classname "Lorg/mariuszgromada/math/mxparser/mXparser;"
  ]
  node [
    id 97
    label "Lorg/mariuszgromada/math/mxparser/mXparser;->setDegreesMode()V [access_flags=public static] @ 0x342040"
    external 0
    entrypoint 0
    methodname "setDegreesMode"
    descriptor "()V"
    accessflags "public static"
    classname "Lorg/mariuszgromada/math/mxparser/mXparser;"
  ]
  node [
    id 98
    label "Lcom/darkempire78/opencalculator/MainActivity;->devideButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b0d4"
    external 0
    entrypoint 1
    methodname "devideButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 99
    label "Lcom/darkempire78/opencalculator/MainActivity;->devideBy100(Landroid/view/View;)V [access_flags=public final] @ 0x24b0fc"
    external 0
    entrypoint 1
    methodname "devideBy100"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 100
    label "Lcom/darkempire78/opencalculator/MainActivity;->eButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b124"
    external 0
    entrypoint 1
    methodname "eButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 101
    label "Lcom/darkempire78/opencalculator/MainActivity;->eightButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b14c"
    external 0
    entrypoint 1
    methodname "eightButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 102
    label "Lcom/darkempire78/opencalculator/MainActivity;->equalsButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b174"
    external 0
    entrypoint 1
    methodname "equalsButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 103
    label "Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;"
    external 1
    entrypoint 0
    methodname "append"
    descriptor "(D)Ljava/lang/StringBuilder;"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 104
    label "Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "(Ljava/lang/String;)V"
    accessflags ""
    classname "Ljava/lang/StringBuilder;"
  ]
  node [
    id 105
    label "Lorg/mariuszgromada/math/mxparser/mXparser;->consolePrintln(Ljava/lang/Object;)V [access_flags=public static] @ 0x34196c"
    external 0
    entrypoint 0
    methodname "consolePrintln"
    descriptor "(Ljava/lang/Object;)V"
    accessflags "public static"
    classname "Lorg/mariuszgromada/math/mxparser/mXparser;"
  ]
  node [
    id 106
    label "Lorg/mariuszgromada/math/mxparser/Expression;->getExpressionString()Ljava/lang/String; [access_flags=public] @ 0x3305b8"
    external 0
    entrypoint 0
    methodname "getExpressionString"
    descriptor "()Ljava/lang/String;"
    accessflags "public"
    classname "Lorg/mariuszgromada/math/mxparser/Expression;"
  ]
  node [
    id 107
    label "Lcom/darkempire78/opencalculator/MainActivity;->exponentButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b3e0"
    external 0
    entrypoint 1
    methodname "exponentButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 108
    label "Lcom/darkempire78/opencalculator/MainActivity;->factorialButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b408"
    external 0
    entrypoint 1
    methodname "factorialButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 109
    label "Lcom/darkempire78/opencalculator/MainActivity;->fiveButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b430"
    external 0
    entrypoint 1
    methodname "fiveButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 110
    label "Lcom/darkempire78/opencalculator/MainActivity;->fourButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b458"
    external 0
    entrypoint 1
    methodname "fourButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 111
    label "Lcom/darkempire78/opencalculator/MainActivity;->invButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b480"
    external 0
    entrypoint 1
    methodname "invButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 112
    label "Landroid/widget/Button;->setText(I)V"
    external 1
    entrypoint 0
    methodname "setText"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/widget/Button;"
  ]
  node [
    id 113
    label "Lcom/darkempire78/opencalculator/MainActivity;->logarithmButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b5f8"
    external 0
    entrypoint 1
    methodname "logarithmButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 114
    label "Lcom/darkempire78/opencalculator/MainActivity;->multiplyButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b634"
    external 0
    entrypoint 1
    methodname "multiplyButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 115
    label "Lcom/darkempire78/opencalculator/MainActivity;->naturalLogarithmButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b65c"
    external 0
    entrypoint 1
    methodname "naturalLogarithmButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 116
    label "Lcom/darkempire78/opencalculator/MainActivity;->nineButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b698"
    external 0
    entrypoint 1
    methodname "nineButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 117
    label "Lcom/darkempire78/opencalculator/MainActivity;->onCreate(Landroid/os/Bundle;)V [access_flags=protected] @ 0x24b6c0"
    external 0
    entrypoint 1
    methodname "onCreate"
    descriptor "(Landroid/os/Bundle;)V"
    accessflags "protected"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 118
    label "Lcom/darkempire78/opencalculator/MainActivity;->setTheme(I)V"
    external 1
    entrypoint 1
    methodname "setTheme"
    descriptor "(I)V"
    accessflags ""
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 119
    label "Landroidx/appcompat/app/AppCompatDelegate;->getDefaultNightMode()I [access_flags=public static] @ 0x10d604"
    external 0
    entrypoint 0
    methodname "getDefaultNightMode"
    descriptor "()I"
    accessflags "public static"
    classname "Landroidx/appcompat/app/AppCompatDelegate;"
  ]
  node [
    id 120
    label "Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V"
    external 1
    entrypoint 0
    methodname "setLayoutTransition"
    descriptor "(Landroid/animation/LayoutTransition;)V"
    accessflags ""
    classname "Landroid/view/ViewGroup;"
  ]
  node [
    id 121
    label "Lcom/darkempire78/opencalculator/MainActivity;->setContentView(I)V"
    external 1
    entrypoint 1
    methodname "setContentView"
    descriptor "(I)V"
    accessflags ""
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 122
    label "Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V"
    external 1
    entrypoint 0
    methodname "setOnLongClickListener"
    descriptor "(Landroid/view/View$OnLongClickListener;)V"
    accessflags ""
    classname "Landroid/widget/ImageButton;"
  ]
  node [
    id 123
    label "Landroid/animation/LayoutTransition;-><init>()V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "()V"
    accessflags ""
    classname "Landroid/animation/LayoutTransition;"
  ]
  node [
    id 124
    label "Landroid/animation/LayoutTransition;->disableTransitionType(I)V"
    external 1
    entrypoint 0
    methodname "disableTransitionType"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/animation/LayoutTransition;"
  ]
  node [
    id 125
    label "Landroid/widget/EditText;->setShowSoftInputOnFocus(Z)V"
    external 1
    entrypoint 0
    methodname "setShowSoftInputOnFocus"
    descriptor "(Z)V"
    accessflags ""
    classname "Landroid/widget/EditText;"
  ]
  node [
    id 126
    label "Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V"
    external 1
    entrypoint 0
    methodname "onCreate"
    descriptor "(Landroid/os/Bundle;)V"
    accessflags ""
    classname "Landroidx/appcompat/app/AppCompatActivity;"
  ]
  node [
    id 127
    label "Lcom/darkempire78/opencalculator/MainActivity;->oneButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b788"
    external 0
    entrypoint 1
    methodname "oneButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 128
    label "Lcom/darkempire78/opencalculator/MainActivity;->openAppMenu(Landroid/view/View;)V [access_flags=public final] @ 0x24b7b0"
    external 0
    entrypoint 1
    methodname "openAppMenu"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 129
    label "Landroid/widget/PopupMenu;-><init>(Landroid/content/Context; Landroid/view/View;)V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "(Landroid/content/Context; Landroid/view/View;)V"
    accessflags ""
    classname "Landroid/widget/PopupMenu;"
  ]
  node [
    id 130
    label "Landroid/view/MenuInflater;->inflate(I Landroid/view/Menu;)V"
    external 1
    entrypoint 0
    methodname "inflate"
    descriptor "(I Landroid/view/Menu;)V"
    accessflags ""
    classname "Landroid/view/MenuInflater;"
  ]
  node [
    id 131
    label "Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;"
    external 1
    entrypoint 0
    methodname "getMenu"
    descriptor "()Landroid/view/Menu;"
    accessflags ""
    classname "Landroid/widget/PopupMenu;"
  ]
  node [
    id 132
    label "Landroid/widget/PopupMenu;->show()V"
    external 1
    entrypoint 0
    methodname "show"
    descriptor "()V"
    accessflags ""
    classname "Landroid/widget/PopupMenu;"
  ]
  node [
    id 133
    label "Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;"
    external 1
    entrypoint 0
    methodname "getMenuInflater"
    descriptor "()Landroid/view/MenuInflater;"
    accessflags ""
    classname "Landroid/widget/PopupMenu;"
  ]
  node [
    id 134
    label "Lcom/darkempire78/opencalculator/MainActivity;->openGithubLink(Landroid/view/MenuItem;)V [access_flags=public final] @ 0x24b7fc"
    external 0
    entrypoint 1
    methodname "openGithubLink"
    descriptor "(Landroid/view/MenuItem;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 135
    label "Lcom/darkempire78/opencalculator/MainActivity;->startActivity(Landroid/content/Intent;)V"
    external 1
    entrypoint 1
    methodname "startActivity"
    descriptor "(Landroid/content/Intent;)V"
    accessflags ""
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 136
    label "Landroid/content/Intent;-><init>(Ljava/lang/String; Landroid/net/Uri;)V"
    external 1
    entrypoint 0
    methodname "<init>"
    descriptor "(Ljava/lang/String; Landroid/net/Uri;)V"
    accessflags ""
    classname "Landroid/content/Intent;"
  ]
  node [
    id 137
    label "Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;"
    external 1
    entrypoint 0
    methodname "parse"
    descriptor "(Ljava/lang/String;)Landroid/net/Uri;"
    accessflags ""
    classname "Landroid/net/Uri;"
  ]
  node [
    id 138
    label "Lcom/darkempire78/opencalculator/MainActivity;->parenthesesButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b838"
    external 0
    entrypoint 1
    methodname "parenthesesButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 139
    label "Ljava/lang/String;->subSequence(I I)Ljava/lang/CharSequence;"
    external 1
    entrypoint 0
    methodname "subSequence"
    descriptor "(I I)Ljava/lang/CharSequence;"
    accessflags ""
    classname "Ljava/lang/String;"
  ]
  node [
    id 140
    label "Lcom/darkempire78/opencalculator/MainActivity;->piButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b95c"
    external 0
    entrypoint 1
    methodname "piButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 141
    label "Lcom/darkempire78/opencalculator/MainActivity;->pointButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b984"
    external 0
    entrypoint 1
    methodname "pointButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 142
    label "Lcom/darkempire78/opencalculator/MainActivity;->scientistModeSwitchButton(Landroid/view/View;)V [access_flags=public final] @ 0x24b9ac"
    external 0
    entrypoint 1
    methodname "scientistModeSwitchButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 143
    label "Landroid/widget/ImageButton;->setImageResource(I)V"
    external 1
    entrypoint 0
    methodname "setImageResource"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/widget/ImageButton;"
  ]
  node [
    id 144
    label "Landroid/widget/TableRow;->setVisibility(I)V"
    external 1
    entrypoint 0
    methodname "setVisibility"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/widget/TableRow;"
  ]
  node [
    id 145
    label "Landroid/widget/TableRow;->getVisibility()I"
    external 1
    entrypoint 0
    methodname "getVisibility"
    descriptor "()I"
    accessflags ""
    classname "Landroid/widget/TableRow;"
  ]
  node [
    id 146
    label "Landroid/widget/TextView;->setVisibility(I)V"
    external 1
    entrypoint 0
    methodname "setVisibility"
    descriptor "(I)V"
    accessflags ""
    classname "Landroid/widget/TextView;"
  ]
  node [
    id 147
    label "Lcom/darkempire78/opencalculator/MainActivity;->selectThemeDialog(Landroid/view/MenuItem;)V [access_flags=public final] @ 0x24bb44"
    external 0
    entrypoint 1
    methodname "selectThemeDialog"
    descriptor "(Landroid/view/MenuItem;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 148
    label "Landroidx/appcompat/app/AlertDialog;->show()V"
    external 1
    entrypoint 0
    methodname "show"
    descriptor "()V"
    accessflags ""
    classname "Landroidx/appcompat/app/AlertDialog;"
  ]
  node [
    id 149
    label "Landroidx/appcompat/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence; I Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder; [access_flags=public] @ 0x108300"
    external 0
    entrypoint 0
    methodname "setSingleChoiceItems"
    descriptor "([Ljava/lang/CharSequence; I Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;"
    accessflags "public"
    classname "Landroidx/appcompat/app/AlertDialog$Builder;"
  ]
  node [
    id 150
    label "Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog; [access_flags=public] @ 0x107d48"
    external 0
    entrypoint 0
    methodname "create"
    descriptor "()Landroidx/appcompat/app/AlertDialog;"
    accessflags "public"
    classname "Landroidx/appcompat/app/AlertDialog$Builder;"
  ]
  node [
    id 151
    label "Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V [access_flags=public constructor] @ 0x108424"
    external 0
    entrypoint 0
    methodname "<init>"
    descriptor "(Landroid/content/Context;)V"
    accessflags "public constructor"
    classname "Landroidx/appcompat/app/AlertDialog$Builder;"
  ]
  node [
    id 152
    label "Lcom/darkempire78/opencalculator/MainActivity;->sevenButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bbc8"
    external 0
    entrypoint 1
    methodname "sevenButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 153
    label "Lcom/darkempire78/opencalculator/MainActivity;->sinusButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bbf0"
    external 0
    entrypoint 1
    methodname "sinusButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 154
    label "Lcom/darkempire78/opencalculator/MainActivity;->sixButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bc2c"
    external 0
    entrypoint 1
    methodname "sixButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 155
    label "Lcom/darkempire78/opencalculator/MainActivity;->squareButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bc54"
    external 0
    entrypoint 1
    methodname "squareButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 156
    label "Lcom/darkempire78/opencalculator/MainActivity;->substractButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bc90"
    external 0
    entrypoint 1
    methodname "substractButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 157
    label "Lcom/darkempire78/opencalculator/MainActivity;->tangentButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bcb8"
    external 0
    entrypoint 1
    methodname "tangentButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 158
    label "Lcom/darkempire78/opencalculator/MainActivity;->threeButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bcf4"
    external 0
    entrypoint 1
    methodname "threeButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 159
    label "Lcom/darkempire78/opencalculator/MainActivity;->twoButton(Landroid/view/View;)V [access_flags=public final] @ 0x24bd1c"
    external 0
    entrypoint 1
    methodname "twoButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  node [
    id 160
    label "Lcom/darkempire78/opencalculator/MainActivity;->zeroButton(Landroid/view/View;)V [access_flags=public final] @ 0x24c050"
    external 0
    entrypoint 1
    methodname "zeroButton"
    descriptor "(Landroid/view/View;)V"
    accessflags "public final"
    classname "Lcom/darkempire78/opencalculator/MainActivity;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 1
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 1
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 1
  ]
  edge [
    source 9
    target 8
  ]
  edge [
    source 10
    target 9
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 1
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 19
    target 1
  ]
  edge [
    source 20
    target 1
  ]
  edge [
    source 21
    target 1
  ]
  edge [
    source 22
    target 1
  ]
  edge [
    source 23
    target 1
  ]
  edge [
    source 24
    target 1
  ]
  edge [
    source 25
    target 1
  ]
  edge [
    source 26
    target 1
  ]
  edge [
    source 27
    target 1
  ]
  edge [
    source 28
    target 1
  ]
  edge [
    source 29
    target 1
  ]
  edge [
    source 30
    target 1
  ]
  edge [
    source 31
    target 1
  ]
  edge [
    source 32
    target 1
  ]
  edge [
    source 33
    target 1
  ]
  edge [
    source 35
    target 1
  ]
  edge [
    source 36
    target 1
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 37
    target 56
  ]
  edge [
    source 37
    target 51
  ]
  edge [
    source 37
    target 57
  ]
  edge [
    source 38
    target 58
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 11
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 15
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 11
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 14
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 49
    target 47
  ]
  edge [
    source 49
    target 48
  ]
  edge [
    source 50
    target 47
  ]
  edge [
    source 50
    target 48
  ]
  edge [
    source 51
    target 47
  ]
  edge [
    source 51
    target 48
  ]
  edge [
    source 52
    target 47
  ]
  edge [
    source 52
    target 48
  ]
  edge [
    source 53
    target 47
  ]
  edge [
    source 53
    target 48
  ]
  edge [
    source 54
    target 47
  ]
  edge [
    source 54
    target 48
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 56
  ]
  edge [
    source 59
    target 51
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 59
    target 50
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 48
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 59
    target 66
  ]
  edge [
    source 59
    target 67
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 69
  ]
  edge [
    source 59
    target 70
  ]
  edge [
    source 59
    target 71
  ]
  edge [
    source 59
    target 72
  ]
  edge [
    source 59
    target 73
  ]
  edge [
    source 59
    target 74
  ]
  edge [
    source 59
    target 75
  ]
  edge [
    source 59
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 79
    target 47
  ]
  edge [
    source 79
    target 82
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 57
  ]
  edge [
    source 84
    target 75
  ]
  edge [
    source 84
    target 67
  ]
  edge [
    source 84
    target 50
  ]
  edge [
    source 84
    target 68
  ]
  edge [
    source 84
    target 59
  ]
  edge [
    source 84
    target 72
  ]
  edge [
    source 84
    target 69
  ]
  edge [
    source 84
    target 55
  ]
  edge [
    source 84
    target 70
  ]
  edge [
    source 84
    target 90
  ]
  edge [
    source 84
    target 89
  ]
  edge [
    source 84
    target 139
  ]
  edge [
    source 84
    target 57
  ]
  edge [
    source 85
    target 50
  ]
  edge [
    source 85
    target 75
  ]
  edge [
    source 85
    target 69
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 55
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 85
    target 59
  ]
  edge [
    source 85
    target 70
  ]
  edge [
    source 85
    target 57
  ]
  edge [
    source 85
    target 90
  ]
  edge [
    source 91
    target 50
  ]
  edge [
    source 91
    target 55
  ]
  edge [
    source 91
    target 56
  ]
  edge [
    source 91
    target 51
  ]
  edge [
    source 91
    target 57
  ]
  edge [
    source 92
    target 84
  ]
  edge [
    source 92
    target 57
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 72
  ]
  edge [
    source 93
    target 59
  ]
  edge [
    source 93
    target 56
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 46
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 93
    target 61
  ]
  edge [
    source 93
    target 49
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 57
  ]
  edge [
    source 98
    target 84
  ]
  edge [
    source 98
    target 57
  ]
  edge [
    source 99
    target 84
  ]
  edge [
    source 99
    target 57
  ]
  edge [
    source 100
    target 84
  ]
  edge [
    source 100
    target 57
  ]
  edge [
    source 101
    target 84
  ]
  edge [
    source 101
    target 57
  ]
  edge [
    source 102
    target 50
  ]
  edge [
    source 102
    target 63
  ]
  edge [
    source 102
    target 64
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 68
  ]
  edge [
    source 102
    target 69
  ]
  edge [
    source 102
    target 61
  ]
  edge [
    source 102
    target 65
  ]
  edge [
    source 102
    target 60
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 67
  ]
  edge [
    source 102
    target 56
  ]
  edge [
    source 102
    target 71
  ]
  edge [
    source 102
    target 55
  ]
  edge [
    source 102
    target 51
  ]
  edge [
    source 102
    target 70
  ]
  edge [
    source 102
    target 62
  ]
  edge [
    source 102
    target 72
  ]
  edge [
    source 102
    target 66
  ]
  edge [
    source 102
    target 88
  ]
  edge [
    source 102
    target 75
  ]
  edge [
    source 102
    target 48
  ]
  edge [
    source 102
    target 57
  ]
  edge [
    source 102
    target 73
  ]
  edge [
    source 102
    target 76
  ]
  edge [
    source 102
    target 106
  ]
  edge [
    source 102
    target 90
  ]
  edge [
    source 102
    target 74
  ]
  edge [
    source 107
    target 84
  ]
  edge [
    source 107
    target 57
  ]
  edge [
    source 108
    target 84
  ]
  edge [
    source 108
    target 57
  ]
  edge [
    source 109
    target 84
  ]
  edge [
    source 109
    target 57
  ]
  edge [
    source 110
    target 84
  ]
  edge [
    source 110
    target 57
  ]
  edge [
    source 111
    target 47
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 57
  ]
  edge [
    source 113
    target 84
  ]
  edge [
    source 113
    target 57
  ]
  edge [
    source 114
    target 84
  ]
  edge [
    source 114
    target 57
  ]
  edge [
    source 115
    target 84
  ]
  edge [
    source 115
    target 57
  ]
  edge [
    source 116
    target 84
  ]
  edge [
    source 116
    target 57
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 119
  ]
  edge [
    source 117
    target 97
  ]
  edge [
    source 117
    target 47
  ]
  edge [
    source 117
    target 120
  ]
  edge [
    source 117
    target 121
  ]
  edge [
    source 117
    target 122
  ]
  edge [
    source 117
    target 50
  ]
  edge [
    source 117
    target 123
  ]
  edge [
    source 117
    target 124
  ]
  edge [
    source 117
    target 125
  ]
  edge [
    source 117
    target 126
  ]
  edge [
    source 117
    target 42
  ]
  edge [
    source 117
    target 5
  ]
  edge [
    source 127
    target 84
  ]
  edge [
    source 127
    target 57
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 128
    target 131
  ]
  edge [
    source 128
    target 132
  ]
  edge [
    source 128
    target 133
  ]
  edge [
    source 128
    target 57
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 136
  ]
  edge [
    source 134
    target 137
  ]
  edge [
    source 134
    target 57
  ]
  edge [
    source 138
    target 84
  ]
  edge [
    source 138
    target 50
  ]
  edge [
    source 138
    target 59
  ]
  edge [
    source 138
    target 69
  ]
  edge [
    source 138
    target 60
  ]
  edge [
    source 138
    target 72
  ]
  edge [
    source 138
    target 61
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 88
  ]
  edge [
    source 138
    target 89
  ]
  edge [
    source 138
    target 57
  ]
  edge [
    source 140
    target 84
  ]
  edge [
    source 140
    target 57
  ]
  edge [
    source 141
    target 84
  ]
  edge [
    source 141
    target 57
  ]
  edge [
    source 142
    target 56
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 94
  ]
  edge [
    source 142
    target 72
  ]
  edge [
    source 142
    target 52
  ]
  edge [
    source 142
    target 144
  ]
  edge [
    source 142
    target 145
  ]
  edge [
    source 142
    target 46
  ]
  edge [
    source 142
    target 49
  ]
  edge [
    source 142
    target 146
  ]
  edge [
    source 142
    target 53
  ]
  edge [
    source 142
    target 57
  ]
  edge [
    source 142
    target 54
  ]
  edge [
    source 147
    target 14
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 147
    target 150
  ]
  edge [
    source 147
    target 2
  ]
  edge [
    source 147
    target 48
  ]
  edge [
    source 147
    target 11
  ]
  edge [
    source 147
    target 57
  ]
  edge [
    source 147
    target 151
  ]
  edge [
    source 152
    target 84
  ]
  edge [
    source 152
    target 57
  ]
  edge [
    source 153
    target 84
  ]
  edge [
    source 153
    target 57
  ]
  edge [
    source 154
    target 84
  ]
  edge [
    source 154
    target 57
  ]
  edge [
    source 155
    target 84
  ]
  edge [
    source 155
    target 57
  ]
  edge [
    source 156
    target 84
  ]
  edge [
    source 156
    target 57
  ]
  edge [
    source 157
    target 84
  ]
  edge [
    source 157
    target 57
  ]
  edge [
    source 158
    target 84
  ]
  edge [
    source 158
    target 57
  ]
  edge [
    source 159
    target 84
  ]
  edge [
    source 159
    target 57
  ]
  edge [
    source 160
    target 84
  ]
  edge [
    source 160
    target 57
  ]
]
