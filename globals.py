import time

_args = None
_node_degrees = {}
_degree_odds = {}
_attribute_odds = {}
_running_timers = {}
_heuristic_timers = {}
_node_attributes = {'g1':{},'g2':{}}
_node_attribute_names = ['descriptor', 'classname','external',\
                    'methodname','entrypoint','accessflags']

def set_args(val):
    """Set the global arguments value.

    Args:
        val: The value to set for arguments.
    """
    global _args
    _args = val

def get_args():
    """ Get the current global arguments value.

    Returns:
        The current value of arguments.
    """
    return _args

def node_attribute_names():
    """ Get the list of node attribute names.

    Returns:
        The list of node attribute names.
    """
    return _node_attribute_names

def set_running_timer(name):
    """Start a timer with the given name.

    Args:
        name: The name of the timer to start.
    """
    global _running_timers
    _running_timers[name] = time.time_ns()

def get_running_timer(name):
    """Get the elapsed time for a running timer by name.

    Args:
        name: The name of the timer.

    Returns:
        The elapsed time in nanoseconds since the timer was started.
    """
    if name in _running_timers.keys():
        return time.time_ns() - _running_timers[name]

def set_heuristic_timer(name):
    """Set or update the heuristic timer for a given heuristic.

    Args:
        name: The name of the heuristic timer to set or update.
    """
    global _heuristic_timers
    if name not in _heuristic_timers.keys():
        _heuristic_timers[name] = get_running_timer(name)
    else:
        _heuristic_timers[name] += get_running_timer(name)

def get_heuristic_timer(name):
    """Get the total elapsed time for a heuristic timer by heuristic name.

    Args:
        name: The name of the heuristic.

    Returns:
        The total elapsed time in nanoseconds for the heuristic timer.
    """
    if name in _heuristic_timers.keys():
        return _heuristic_timers[name]

def set_node_degrees(graph_name, node_dict):
    global _node_degrees
    _node_degrees[graph_name] = node_dict
    

def get_node_degrees(graph_name, node=None):
    if node:
        return _node_degrees[graph_name][node]
    else:
        return _node_degrees[graph_name]

def set_degree_odds(val):
    """Set the dictionary containing the frequency for each degree.

    Args:
        val: The value to set for degree odds.
    """
    global _degree_odds
    _degree_odds = val

def get_degree_odds(degree=None):
    """Get the dictionary containing the frequency for each degree.

    Returns:
        The current value of degree odds.
    """
    if degree:
        return _degree_odds[degree]
    else:
        return _degree_odds

def set_node_attributes(graph_name, att_dictionary, att_name):
    """Create the disctionary of attribute values for a given graph
    Set the attributes for nodes in a graph.

    Args:
        graph_name: The name of the graph.
        att_dictionary: The dictionary containing attribute data for the nodes.
        att_name: The name of the attribute to set.
    """
    global _node_attributes
    _node_attributes[graph_name][att_name] = att_dictionary

def get_node_attributes(graph_name, attribute_name, node=None):
    """ Get the attributes & values thereof for nodes in a graph.

    Args:
        graph_name: The name of the graph.
        attribute_name: The name of the attribute to retrieve.
        node: The specific node to retrieve attributes for (optional).

    Returns:
        The attribute data for the specified node or all nodes if no node is specified.
    """
    if node:
        return _node_attributes[graph_name][attribute_name][node]
    else:
        return _node_attributes[graph_name][attribute_name]

def set_attribute_odds(attribute, frequency_dict):
    """Set the odds for a specific attribute using a frequency dictionary.

    Args:
        attribute: The attribute for which to set the odds.
        frequency_dict: The dictionary containing frequency data for the attribute.
    """
    global _attribute_odds
    _attribute_odds[attribute] = frequency_dict

def get_attribute_odds(attribute, value):
    """Get the odds for a specific attribute value.

    Args:
        attribute: The attribute to retrieve odds for.
        value: The specific value of the attribute.

    Returns:
        The odds for the given attribute value.
    """
    return _attribute_odds[attribute][value]
