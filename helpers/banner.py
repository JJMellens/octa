import os

def get_banner(max_taco):
    """Prints a TACO. Essential to program functionality."""
    terminal_width = os.get_terminal_size().columns
    rounded_width = min(200, (terminal_width // 10) * 10)
    if not max_taco:
        rounded_width = min(100,rounded_width)
    try:
        with open(f'media/taco-{rounded_width}.txt', 'r') as file:
            ascii_art = file.read()
        return ascii_art
    except FileNotFoundError:
        print(f"No ASCII art file found for width {rounded_width}")
    return ""