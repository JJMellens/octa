import os
import subprocess

def generate_call_graph(files):
    """Generate call graphs for a list of APK files using Androguard.
    
    This function processes each file name in the provided list,
    checks if the corresponding APK file exists in the "data" directory,
    and then generates a call graph for that APK using Androguard.
    The call graph is saved in GML format in the "data" directory.

    If an APK file does not exist in the "data" directory,
    the function exits with an error message.
    The Androguard command is executed in the current directory of the script.

    Parameters
    ----------
    files : list of str
        A list of file names (without extension)
        for which call graphs are to be generated.

    Returns
    -------
    None
    """
    for name in files:
        if os.path.exists(f"data/{name}.apk"):
            cmd = ['androguard', 'cg', f"data/{name}.apk", '-o', f"data/{name}.gml"]
            subprocess.run(cmd, shell=False, cwd=os.getcwd())
        else:
            exit(code=f"Error in androguard: {name} is not a valid path.")
    return