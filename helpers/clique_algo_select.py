import salsa
from globals import get_args
import networkx as nx

def select_clique_algorithm(g1, g2, g3):
    """Select and run a clique-finding algorithm based on the provided arguments.
    
    Selects a clique-finding algorithm based on the `args.algorithm`
    and runs it on the input graphs `g1` and `g2`, or their MPG `g3`.

    Exits with an error if `args.algorithm` is not recognized.

    Parameters
    ----------
    g1 : networkx.Graph
        The first input graph.
    g2 : networkx.Graph
        The second input graph.
    g3 : networkx.Graph
        Modular product graph of g1 and g2

    Returns
    -------
    list
        The maximum clique found by the selected algorithm.
    """
    algorithm = get_args().algorithm
    if algorithm == 'apx':
        maximum_clique = list(nx.algorithms.approximation.max_clique(g3))
    elif algorithm == 'salsa':
        maximum_clique = salsa.Salsa(g1,g2).run()
    else:
        exit(f"Error: {algorithm} is not a valid clique-finding algorithm")

    return maximum_clique