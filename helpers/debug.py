from globals import get_args
from globals import set_heuristic_timer
from globals import get_heuristic_timer
from globals import set_running_timer
from globals import get_running_timer
from globals import node_attribute_names

def debug_print(statement_level, statement):
    """Prints a statement if the correct debug level is reached.
    Added for keeping debug statements smaller.
    """
    debug_level = get_args().debug
    if debug_level >= statement_level:
        print(statement)
    return

def debug_timer(statement_level, action, name):
    debug_level = get_args().debug
    if debug_level >= statement_level:
        if action in ['start','set']:
            set_running_timer(name)

        elif action == 'stop':
            set_heuristic_timer(name)

        elif action == 'time':
            print(f"{name} runtime:\
            {round(get_running_timer(name)/1000000,3)} ms")
            
        elif action == 'print':
            names = [name]
            if name == 'all':
                names = node_attribute_names().copy()
                names.append('degree')
                # names.append('connectivity')
            for name in names:
                print(f"{name} Total time:\
                {round(get_heuristic_timer(name)/1000000,3)} ms")
    