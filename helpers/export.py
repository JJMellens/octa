import csv
import pickle
from globals import get_args

def export_clique(sfx, maximum_clique):
    """Export the maximum clique to a file in both pickle and CSV formats.
    
    Exports the maximum clique to a pickle file and/or a CSV file. 
    Adapts output format based on if `--line_graph` is set.

    Parameters
    ----------
    sfx : str
        Suffix for the output file names.
    maximum_clique : list of tuple
        The maximum clique containing either node pairs or edge pairs.

    Returns
    -------
    None
    """
    args = get_args()
    if args.write_out:
        with open(f"intermediates/max_clique_of_{args.gml_1}_and_{args.gml_2}"+
                f"{sfx}.pickle", "wb") as f:
            pickle.dump(maximum_clique, f)

    if args.line_graph:
        with open(f"output/max_clique_of_{args.gml_1}_and_{args.gml_2}"+
                f"{sfx}.csv", 'w') as f:
            csv_out=csv.writer(f)
            csv_out.writerow([f'{args.gml_1}',f'{args.gml_2}'])
            for edge_pair in list(maximum_clique):
                for pair in edge_pair:
                    csv_out.writerow(pair[0],pair[1])

    else:
        with open(f"output/max_clique_of_{args.gml_1}_and_{args.gml_2}"+
                f"{sfx}.csv", 'w') as f:
            csv_out=csv.writer(f)
            csv_out.writerow([f'{args.gml_1}',f'{args.gml_2}'])
            [csv_out.writerow(node) for node in list(maximum_clique)]