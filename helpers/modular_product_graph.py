import networkx as nx
import pickle
import time
from helpers.time import time_passed
from globals import get_args

def get_modular_product_graph(g1, g2, sfx):
    """Get or create the modular product graph of two input graphs.
    
    Either creates a new modular product graph (MPG) of `g1` and `g2`
    or loads an existing one from a file, depending on the `args` provided. 

    If neither the modular product graph nor the maximum clique is provided,
    it generates the MPG and optionally writes it to a file.

    If the MPG is provided, it loads the graph from the specified file.

    Parameters
    ----------
    g1 : networkx.Graph
        The first input graph.
    g2 : networkx.Graph
        The second input graph.
    sfx : str
        Suffix for the output file names.

    Returns
    -------
    networkx.Graph or None
        The modular product graph `g3` if created or loaded, otherwise `None`.
    """
    args = get_args()
    if not args.module_product_graph:
        print("Starting with modular graph creation, this will take a while.")
        premodular_time = time.time()
        g3 = nx.modular_product(g1, g2)
        if args.write_out:
            with open(f"intermediates/mpg_of_{args.gml_1}_and_{args.gml_2}"+
                        f"{sfx}.pickle", 'wb') as f:
                pickle.dump(g3, f)
        print(f"Created Modular Product Graph in "+
                f"{time_passed(premodular_time)} seconds")
    
    elif not args.max_clique:
        print(f"Using provided modular product graph intermediates/"+
                f"{args.mpg}.pickle, skipping mpg generation.")
        with open(f"intermediates/{args.mpg}.pickle", "rb") as f:
            g3 = pickle.load(f)
    
    else:
        print("Both modular graph and maximum clique provided; "+
                "skipping loading graph into memory.")
        g3 = None
    return g3