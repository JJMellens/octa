import argparse

def taco_parser():
    """Create an argument parser for the TACOS application.
    
    Returns
    -------
    argparse.ArgumentParser
        The argument parser with the defined arguments and options.
    """
    parser = argparse.ArgumentParser(
        prog='tacos',
        description=" Identify overlap in two versions "+
            "of an obfuscated application. "+
            "To use, place two .gml files in the data folder.",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument\
        ('gml_1',
         help="Name of the .gml file containing graph g1")
    parser.add_argument\
        ('gml_2', 
         help="Name of the .gml file containing graph g2")
    parser.add_argument\
        ('-d', '--debug',
         choices = [0,1,2,3,4],
         type=int,
         default=1,
         help="Set debug level.Choose between the following: \n\
        0 :     error\n\
        1 :     info\n\
        2 :     timing\n\
        3 :     debug\n\
        4 :     super debug")
    parser.add_argument\
        ('-m', '--module_product_graph',
         help="Name of the .gml file containing the modular product graph " +
            "of the provided graphs. Skips modular graph creation.")
    parser.add_argument\
        ('-l', '--line_graph', 
         action = 'store_true',
         help="Turn g1 and g2 into line graphs first. "+
            "Significantly increases memory usage for "+
            "modular graph creation, but speeds up maximum clique finding.")
    parser.add_argument\
        ('-c', '--max_clique', \
         help="Name of file containing the maximum clique for the "+
            "modular product graph of g1 and g2. "+
            "Skips calculation of the mpg and maximum clique.")
    parser.add_argument\
        ('-s', '--strip_leaves',
         action = 'store_true',
         help="Remove leaf nodes from g1 and g2 to speed up calculations.")
    parser.add_argument\
        ('-t', '--top_hub_percent',
         type = float,
         help="Top percentage of hub nodes to check for matching pairs. "+
         "Higher values increase matches found at the cost of speed. "+
         "Set between 0 and 1. Default .2",
         default=.2)
    parser.add_argument\
        ('-w', '--write_out',
         action = 'store_true',
         help="Write out intermediate results to protect against data loss.")
    parser.add_argument\
        ('-A', '--algorithm',
         default='salsa',
         help="Select clique finding algorithm. "+
         "Choose between the following: \n\
        apx         approximate networkx algorithm\n\
        salsa       Custom SALSA algorithm (default)\n",
        choices=['apx','salsa'])
    parser.add_argument('-a', '--alpha', \
         default=0.01,
         type=float,
         help="Maximum collission chance when using SALSA. Default is 0.01")
    parser.add_argument\
        ('-g', '--generate_call_graph',
         action='store_true',
         help='Indicate call graphs must be generated first. '+
            'Assumes gml_1 and gml_2 are .apk files instead.')
    parser.add_argument\
        ('-M', '--max_taco',
         action='store_true',
         help='Enable maximal taco')
    return parser