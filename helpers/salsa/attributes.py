import networkx as nx
from globals import node_attribute_names
from globals import set_node_attributes
from globals import get_node_attributes
from globals import set_attribute_odds
from globals import get_attribute_odds
from helpers.debug import debug_timer

def calculate_attribute_frequencies(value_list, graph):
    """
    Calculate the frequency and odds of each attribute in a given list 
    relative to the total number of nodes in the graph.
    
    Parameters
    ----------
    value_list : dict
        A dictionary where the keys are nodes and the values are attributes.
    graph : networkx.Graph
        The graph from which the nodes are derived. The total number of nodes in
        the graph is used to calculate odds.
    
    Returns
    -------
    dict
        A dictionary where the keys are attributes and the values are the odds 
        of each attribute occurring in the graph.
    """
    value_counts = {}
    for node, value in value_list.items():
        if value not in value_counts:
            value_counts.update({value:1})
        else:
            value_counts[value] = value_counts[value]+1
    value_odds = {}
    total_nodes = len(graph.nodes())
    for value, frequency in value_counts.items():
        value_odds[value] = frequency/total_nodes
    return value_odds

def check_attribute_match(g1_can, g2_can, att_name):
    """Check if the attribute of the candidate nodes match.
    
    Arguments
    ---------
    g1_can : node
        Candidate node in g1
    g2_can : node
        Candidate node in g2
    att_name : str
        The name of the attribute to check

    Returns
    -------
    float
        The likelihood of the nodes being a match based on the attribute.
    """
    debug_timer(4,'start',att_name)
    odds = 1
    g1_att_value = get_node_attributes('g1',att_name,g1_can)
    g2_att_value = get_node_attributes('g2',att_name,g2_can)
    if g1_att_value == g2_att_value:
        odds = get_attribute_odds(att_name,g1_att_value)
    debug_timer(4,'stop',att_name)
    return odds


def populate_attribute_table(g1, g2):
    """
    Populate the attribute table with frequency and odds values for node attributes
    from two graphs.

    Performs the following steps:
    1. Sets the node attribute values for both graphs.
    2. Calculates the frequencies for each value of each attribute in both graphs.
    3. Combines the frequencies from both graphs to calculate overall odds.
    4. Sets the combined attribute odds for each attribute value.

    Parameters
    ----------
    g1 : networkx.Graph
        The first graph whose node attributes are used for calculation.
    g2 : networkx.Graph
        The second graph whose node attributes are used for calculation.
    """

    # Set node attribute values
    for g in [g1, g2]:
        g_name = 'g1' if g == g1 else 'g2'
        for att_name in node_attribute_names():
            att_dict = nx.get_node_attributes(g, att_name)
            set_node_attributes(g_name, att_dict, att_name)

    # Set attribute frequency values
    for att_name in node_attribute_names():
        att_dict = get_node_attributes('g1', att_name)
        att_prob_dict1 = calculate_attribute_frequencies(att_dict, g)
        att_dict = get_node_attributes('g2', att_name)
        att_prob_dict2 = calculate_attribute_frequencies(att_dict, g)

        total_att_prob_dict = att_prob_dict1
        for att_value, odds in att_prob_dict2.items():
            if att_value in total_att_prob_dict:
                total_att_prob_dict[att_value] *= odds
        set_attribute_odds(att_name, total_att_prob_dict)