from helpers.debug import debug_timer
import networkx as nx
from collections import Counter

def count_neighbors_in_set(graph, node, node_set):
    """Count the number of neighbors of a node in a given set of nodes.
    
    Parameters
    ----------
    graph : networkx.Graph
        The input graph.
    node : node
        The node whose neighbors are to be counted.
    node_set : set
        The set of nodes to count the neighbors from.
    
    Returns
    -------
    int
        The number of neighbors in the given set.
    """
    neighbors = set(graph.neighbors(node))
    count = len(neighbors.intersection(node_set))
    return count

def calculate_neighbors_count_probabilities(graph, node_set):
    """Calculate the probabilities of the number of neighbors
    in a given set for each node in the graph.
    
    Parameters
    ----------
    graph : networkx.Graph
        The input graph.
    node_set : set
        The set of nodes to count the neighbors from.
    
    Returns
    -------
    dict
        A dictionary where the keys are the counts of neighbors in the set
        and the values are the probabilities of those counts.
    """
    neighbors_count = [count_neighbors_in_set(graph, node, node_set)\
                       for node in graph.nodes()]
    count_frequency = Counter(neighbors_count)
    total_nodes = len(graph.nodes())
    
    count_probabilities = {count: freq / total_nodes for count,\
                            freq in count_frequency.items()}
    return count_probabilities

def likelihood_of_x_neighbors(g1, g1_nodes, g2, g2_nodes, x):
    """Calculate the likelihood that any given pair of nodes(one from each graph)
    are neighbors of exactly x nodes from g1_nodes and g2_nodes respectively.
    
    Parameters
    ----------
    g1 : networkx.Graph
        The first graph.
    g1_nodes : set
        The set of nodes in the first graph to count the neighbors from.
    g2 : networkx.Graph
        The second graph.
    g2_nodes : set
        The set of nodes in the second graph to count the neighbors from.
    x : int
        The target number of neighbors.
    
    Returns
    -------
    float
        The likelihood that a random node from g1 and a random node from g2
        have exactly x neighbors from g1_nodes and g2_nodes respectively.
    """
    neighbors_count_prob_g1 = calculate_neighbors_count_probabilities\
        (g1, g1_nodes)
    neighbors_count_prob_g2 = calculate_neighbors_count_probabilities\
        (g2, g2_nodes)
    
    prob_g1 = neighbors_count_prob_g1.get(x, 0)
    prob_g2 = neighbors_count_prob_g2.get(x, 0)
    
    combined_probability = prob_g1 * prob_g2
    
    return combined_probability
    
def check_connectivity_match(g1, g2, g1_nodes, g2_nodes, g1_can, g2_can):
    """Check if connectivity of candidate nodes in g1 matches with g2.
    
    Connectivity refers to degree with which neighbors of candidate nodes
    connect to already matched nodes.

    Arguments
    ---------
    self.g1 : networkx.Graph
        The first graph
    self.g2 : networkx.Graph
        The second graph
    self.g1_nodes : list
        List of nodes in g1 that have been matched
    self.g2_nodes : list
        List of nodes in g2 that have been matched
    self.g1_can : node
        Candidate node in g1
    self.g2_can : node
        Candidate node in g2

    Returns
    -------
    float
        The likelihood of the match being random chance.
    """
    debug_timer(4,'start','connectivity')
    odds = 1
    g1_can_connectivity = count_neighbors_in_set\
        (g1, g1_can, g1_nodes)
    if g1_can_connectivity > 1:
        g2_can_connectivity = count_neighbors_in_set\
            (g2, g2_can, g2_nodes)
        if g1_can_connectivity == g2_can_connectivity:
            odds = likelihood_of_x_neighbors\
                (g1, g1_nodes, g2, g2_nodes,
                    g1_can_connectivity)
    debug_timer(4,'stop','connectivity')
    return odds