from globals import get_degree_odds
from globals import set_degree_odds
from globals import set_node_degrees
from globals import get_node_degrees
from helpers.debug import debug_timer
import networkx as nx
from collections import Counter

def calculate_degree_probabilities(graph):
    """Calculate the degree probabilities for a graph.
    
    Parameters
    ----------
    graph : networkx.Graph
        The input graph.
    
    Returns
    -------
    dict
        A dictionary where the keys are degree
        and the values are the probabilities of those degrees.
    """
    degrees = [degree for node, degree in graph.degree()]
    degree_count = Counter(degrees)
    total_nodes = len(graph.nodes())
    
    degree_probabilities = {degree: count / total_nodes for degree,\
                             count in degree_count.items()}
    return degree_probabilities

def check_degree_match(g1_can, g2_can):
    """Check if the degree of the candidate nodes in g1 matches with g2.
    
    Arguments
    ---------
    g1 : networkx.Graph
        The first graph
    g2 : networkx.Graph
        The second graph
    g1_can : node
        Candidate node in g1
    g2_can : node
        Candidate node in g2

    Returns
    -------
    float
        The likelihood of the nodes being a match based on degree.
    """
    debug_timer(3,'start','degree')
    odds = 1
    target_value = get_node_degrees('g1', g1_can)
    if target_value == get_node_degrees('g2', g2_can):
        odds = get_degree_odds(target_value)
    debug_timer(3,'stop','degree')
    return odds

def populate_degree_table(g1, g2):
    """
    Populate the degree table with combined degree probabilities from two graphs.

    Performs the following steps:
    1. Calculates the degree probabilities for each graph.
    2. Multiplies each graph's probabilities to find the overall oods per degree.
    3. Sets the combined degree probabilities.
    
    Parameters
    ----------
    g1 : networkx.Graph
        The first graph whose degree probabilities are used for calculation.
    g2 : networkx.Graph
        The second graph whose degree probabilities are used for calculation.

    Returns
    -----
    None
    """
    for g in [g1, g2]:
        g_name = 'g1' if g == g1 else 'g2'
        degree_dict = {}
        for node in g.nodes():
            degree_dict[node] = nx.degree(g, node)
        set_node_degrees(g_name, degree_dict)

    degree_prob_g1 = calculate_degree_probabilities(g1)
    degree_prob_g2 = calculate_degree_probabilities(g2)
    total_degree_prob = degree_prob_g1
    for degree, odds in degree_prob_g2.items():
        if degree in total_degree_prob:
            total_degree_prob[degree] = total_degree_prob[degree]*odds
    set_degree_odds(total_degree_prob)