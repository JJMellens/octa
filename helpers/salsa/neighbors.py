import networkx as nx
from collections import Counter

def sum_of_neighbors_degrees(graph, node):
    """
    Calculate the sum of the degrees of the neighbors for a given node in the graph.
    
    Parameters:
    graph (networkx.Graph): The input graph.
    node: The node whose neighbors' degrees are to be summed.
    
    Returns:
    int: The sum of the degrees of the neighbors of the node.
    """
    neighbors = list(graph.neighbors(node))
    neighbor_degrees = [graph.degree(neighbor) for neighbor in neighbors]
    sum_degrees = sum(neighbor_degrees)
    return sum_degrees

def calculate_neighbors_degree_sums(graph):
    """
    Calculate the sum of the degrees of the neighbors for each node in the graph.
    
    Parameters:
    graph (networkx.Graph): The input graph.
    
    Returns:
    list: A list of sums of the degrees of the neighbors for each node.
    """
    return [sum_of_neighbors_degrees(graph, node) for node in graph.nodes()]

def calculate_degree_sum_probabilities(graph):
    """
    Calculate the probabilities of each sum of neighbors' degrees in the graph.
    
    Parameters:
    graph (networkx.Graph): The input graph.
    
    Returns:
    dict: A dictionary where the keys are sums of neighbors' 
      degrees and the values are the probabilities of those sums.
    """
    degree_sums = calculate_neighbors_degree_sums(graph)
    degree_sum_count = Counter(degree_sums)
    total_nodes = len(graph.nodes())
    
    degree_sum_probabilities = {degree_sum: count / total_nodes for degree_sum, count in degree_sum_count.items()}
    return degree_sum_probabilities

def likelihood_of_neighbors_degree_sum(graph1, graph2, target_sum):
    """Calculate the likelihood that the sum of the degrees of the neighbors
    of any two random nodes (one from each graph) is equal to the target sum.
    
    Parameters:
    graph1 (networkx.Graph): The first graph.
    graph2 (networkx.Graph): The second graph.
    target_sum (int): The target sum to check for.
    
    Returns:
    float: The likelihood that the sum of the degrees of the neighbors of a random node from graph1
           and a random node from graph2 is equal to the target sum.
    """
    degree_sum_prob_g1 = calculate_degree_sum_probabilities(graph1)
    degree_sum_prob_g2 = calculate_degree_sum_probabilities(graph2)
    
    # Calculate the combined probability for the target sum
    combined_probability = 0.0
    for sum1 in degree_sum_prob_g1:
        sum2 = target_sum - sum1
        if sum2 in degree_sum_prob_g2:
            combined_probability += degree_sum_prob_g1[sum1] * degree_sum_prob_g2[sum2]
    
    return combined_probability

