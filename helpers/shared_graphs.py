import networkx as nx
from globals import get_args
from helpers.verify import count_floating_nodes

def generate_shared_graphs(g1, g2, maximum_clique):
    """Generate shared subgraphs of two input graphs based on the maximum clique.
    
    Mdifies the input graphs `g1` and `g2` to retain only the nodes
    that are part of the maximum clique. Nodes not part of the maximum clique
    are removed from the graphs. If `line_graph` is set, it processes
    the maximum clique as edge pairs; otherwise, it processes it as node pairs.

    Parameters
    ----------
    g1 : networkx.Graph
        The first input graph.
    g2 : networkx.Graph
        The second input graph.
    maximum_clique : list of tuple
        The maximum clique containing either node pairs or edge pairs.

    Returns
    -------
    None
    """
    g1_nodes = set()
    g2_nodes = set()
    if get_args().line_graph:
        for edge_pair in list(maximum_clique):
            [g1_nodes.add(node) for node in edge_pair[0]]
            [g2_nodes.add(node) for node in edge_pair[1]]
    else:
        [g1_nodes.add(node[0]) for node in list(maximum_clique)]
        [g2_nodes.add(node[1]) for node in list(maximum_clique)]
    g1.remove_nodes_from(list(set(g1.nodes) - g1_nodes))
    g2.remove_nodes_from(list(set(g2.nodes) - g2_nodes))

    count_floating_nodes(g1, g2, maximum_clique)