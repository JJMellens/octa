from math import floor
import time
    
def time_passed(start_time):
    """Calculate the time passed since the given start time
    and return it in a formatted string.
    
    Parameters
    ----------
    start_time : float
        The start time in seconds since the epoch (as returned by `time.time()`)
    
    Returns
    -------
    str
        A string representing the time passed in the format "HH:MM:SS"
    """
    passed_time = time.time()-start_time
    hours = floor(passed_time/3600)
    minutes = floor(passed_time%3600/60)
    seconds = floor(passed_time%60)

    return f"{hours}H:{minutes}M:{seconds}S"