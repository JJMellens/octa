from helpers.debug import debug_print
import networkx as nx

def verify_uniqueness(matching_pairs):
    """"""
    A = []
    B = []
    for a, b in matching_pairs:
        A.append(a)
        B.append(b)
    A_is_unique = True if len(A) == len(set(A)) else False
    B_is_unique = True if len(B) == len(set(B)) else False
    if A_is_unique and B_is_unique:
        debug_print(2,f"No duplicates in matching pairs.")
        return True
    else:
        debug_print(0,f"ERROR: Duplicates detected in matching pairs:\n"+
                    f"A duplicates: {len(A)-len(set(A))} out of {len(A)}\n"
                    f"A duplicates: {len(B)-len(set(B))} out of {len(B)}\n")
        return False
    
def count_floating_nodes(g1, g2, matching_nodes):
    floating_nodes = 0
    for g1_node, g2_node in matching_nodes:
        if nx.degree(g1, g1_node) == 0 or \
        nx.degree(g2, g2_node) == 0:
            floating_nodes += 1
    debug_print(1, f"Floating nodes in shared graphs: {floating_nodes}")
    return floating_nodes