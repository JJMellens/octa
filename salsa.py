from helpers.salsa import connectivity as conn
from helpers.salsa import degree
from helpers.salsa import attributes as att
from helpers.time import time_passed
from helpers.debug import debug_print
from helpers.debug import debug_timer
from helpers.verify import verify_uniqueness
from globals import get_args
from globals import node_attribute_names
import networkx as nx
import time

class Salsa():
    """
    Implements SALSA (Subgraph Approximation using Label and Structure Analysis).

    Performs fuzzy matching using various attributes of the nodes,
    and assumes they match if the likelihood of those two nodes having
    the same attributes is less than a given value alpha (default 1%).

    Attributes
    ----------
    g1 : networkx.Graph
        The first graph.
    g2 : networkx.Graph
        The second graph.
    g1_nodes : list
        List of matched nodes in the first graph.
    g2_nodes : list
        List of matched nodes in the second graph.
    g1_remaining : networkx.Graph
        Copy of the first graph with unmatched nodes.
    g2_remaining : networkx.Graph
        Copy of the second graph with unmatched nodes.
    start_time : float
        Timestamp when the analysis started.

    Methods
    -------
    initial_name_match()
        Find matching nodes based on node names.
    check_connectivity_match()
        Check if the connectivity of the candidate nodes in g1 matches with g2.
    check_degree_match()
        Check if the degree of the candidate nodes in g1 matches with g2.
    check_attribute_match(att_name)
        Check if the attribute of the candidate nodes match.
    find_max_clique()
        Find the maximum clique of matching node pairs between two graphs.
    """

    def __init__(self, g1,g2):
        """
        Parameters
        ----------
        graph1 : networkx.Graph
            The first graph.
        graph2 : networkx.Graph
            The second graph.
        
        Returns
        -------
        list of tuple
            List of tuples representing the initial matching pairs of nodes.
        """
        self.g1 = g1
        self.g2 = g2
        self.g1_nodes = []
        self.g2_nodes = []
        self.checked_pairs = []
        self.matching_pairs = []
        self.start_time = time.time()

    def initial_name_match(self):
        """Find matching nodes based on node names.
        
        Only checks nodes with names longer than 4 characters.

        Returns
        -------
        list of tuple
            List of matching node pairs.
        """
        nodes_g1 = [node for node in self.g1.nodes() if len(str(node)) > 4]
        nodes_g2 = [node for node in self.g2.nodes() if len(str(node)) > 4]
        self.new_pair_matches = []
        
        for g1_can in nodes_g1:
            for g2_can in nodes_g2:
                if g1_can == g2_can:
                    if g1_can not in self.g1_nodes and \
                        g2_can not in self.g2_nodes:
                        self.g1_nodes.append(g1_can)
                        self.g2_nodes.append(g2_can)
                        self.process_new_match((g1_can,g2_can))
                        #self.matching_pairs.append((g1_can, g2_can))
                        break

        debug_print(2, f"Found {len(self.matching_pairs)}"+
                    f" matching pairs in {time_passed(self.start_time)}")
        return self.matching_pairs
        
    def process_new_match(self, best_match):
        """Append the found match to the list of matched nodes
        and update the remaining nodes.
        
        Parameters
        ----------
        potential_match : tuple
            Tuple of nodes to be added to matched pairs

        Returns
        -------
        None
        """
        self.g1_nodes.append(best_match[0])
        self.g2_nodes.append(best_match[1])
        self.matching_pairs.append(best_match)
        self.new_pair_matches.append(best_match)
        return
    
    def populate_lookup_tables(self):
        """Populate lookup tables for collision likelihood calculations.
        
        Returns
        -------
        None
        """
        debug_print(2,"Building lookup tables")
        att.populate_attribute_table(self.g1, self.g2)
        degree.populate_degree_table(self.g1, self.g2)
        debug_print(2,"Finished building lookup tables")
        return
        
    def heuristics_match(self, g1_can, g2_can):   
        """Evaluate the likelihood of a match between candidate nodes
        using various heuristics.

        Parameters
        ----------
        g1_can : node
            Candidate node in g1
        g2_can : node
            Candidate node in g2
        
        Returns
        -------
        bool
            If nodes match and likelihood of collission is below alpha
        """
        likelihood = 1
        likelihoods = []  
        
        # likelihoods.append(('connectivity', conn.check_connectivity_match\
        #     (self.g1, self.g2, self.g1_nodes, self.g2_nodes, g1_can, g2_can)))
        likelihoods.append(('degree', degree.check_degree_match(g1_can, g2_can)))        
        
        for attribute in node_attribute_names():
            likelihoods.append((attribute, att.check_attribute_match\
                (g1_can, g2_can, attribute)))
            
        for property, chance in likelihoods:
            likelihood *= chance
            
        # Debug statements    
        for property, chance in likelihoods:
            debug_print(4,f"{property} collission chance: "\
                    f"{round(chance*100,2)}%")
        debug_print(4,f"Pre-adjusted likelihood: {round(likelihood,8)}")
        likelihood = (1/(len(self.g1.nodes())*len(self.g2.nodes())))/likelihood
        debug_print(4,f"Actual likelihood: {round(likelihood,8)}")

        if likelihood <= get_args().alpha and \
            likelihood < self.best_g2_can_likelihood:
            self.best_g2_can_likelihood = likelihood
            return True 
        else:
            return False
        
        
    def iterative_growth(self, pairs_to_check):
        """Find the maximum clique of matching node pairs between two graphs.
        
        Identifies the largest set of matching node pairs between two graphs
        by initially matching nodes by label, and then iteratively expanding
        the set of matched pairs using heuristics that check connectivity,
        degree, and all node attributes.

        Returns
        -------
        list of tuple
            List of tuples representing the matched node pairs.
        """

        while True:
            round_start = time.time()
            pairs_added_this_round = 0
            self.new_pair_matches = []
            pairs_checked_this_round = []
            # Iterate over candidate pairs
            debug_print(3,f"Checking {len(pairs_to_check)} pairs")

            for node1, node2 in pairs_to_check:
                debug_timer(3,'start','known_pair_check')
                self.checked_pairs.append((node1,node2))
                pairs_checked_this_round.append((node1,node2))
                g1_candidates = nx.all_neighbors(self.g1, node1)
                g2_candidates = nx.all_neighbors(self.g2, node2)

                for g1_can in g1_candidates:
                    best_match = None
                    self.best_g2_can_likelihood = 1
                    if g1_can not in self.g1_nodes:
                        for g2_can in g2_candidates:
                            if g2_can not in self.g2_nodes:
                                if self.heuristics_match(g1_can, g2_can):
                                    best_match = (g1_can, g2_can) 

                    if best_match:
                        pairs_added_this_round += 1
                        debug_print(4,f"Match for {g1_can} collision chance: "+
                            f"{round(self.best_g2_can_likelihood,8)}")
                        self.process_new_match(best_match)

            for pair in self.new_pair_matches:
                pairs_to_check.append(pair)
            
            for pair in pairs_checked_this_round:
                pairs_to_check.remove(pair)
                
            debug_timer(4,'stop','known_pair_check')
            debug_timer(4,'print','known_pair_check')
            debug_timer(4,'print','all')
            if pairs_added_this_round > 0:
                debug_print(2,f"Checked {len(pairs_checked_this_round)} pairs")
                n = 'pairs' if pairs_added_this_round > 1 else 'pair'
                debug_print(2,f"Added {pairs_added_this_round} node {n} "+
                      f"in {time_passed(round_start)}. Continuing")
            else:
                debug_print(2, f"No more nodes found. SALSA finished.")
                break

        return self.matching_pairs

    def find_hub_nodes(self):
        """Find top n nodes with highest connectivity for next expansion wave.
        """
        hub_nodes = []
        top_percent = min(1, abs(get_args().top_hub_percent))
        top_n = round(min(self.g1.number_of_nodes(), self.g2.number_of_nodes())*
                      top_percent,0)
        g1_sorted_nodes = sorted(self.g1.degree, key=lambda x: x[1], reverse=True)
        g2_sorted_nodes = sorted(self.g2.degree, key=lambda x: x[1], reverse=True)
        for i, j in zip(g1_sorted_nodes[:int(top_n)],g2_sorted_nodes[:int(top_n)]):
            hub_nodes.append((i[0],j[0]))
        
        return hub_nodes

    def run(self):
        """Execute the SALSA algorithm.""" 
        self.populate_lookup_tables()

        # Initial match
        pairs_to_check = self.initial_name_match().copy()

        # Select hub nodes to add for expansion wave
        hubs_added = 0
        for pair in self.find_hub_nodes():
            if pair not in self.checked_pairs:
                hubs_added += 1
                pairs_to_check.append(pair)
        debug_print(2,f"Adding additional {hubs_added} hub pairs to check.")

        # Rapid expansion
        self.matching_pairs = self.iterative_growth(pairs_to_check)


        verify_uniqueness(self.matching_pairs)
       
            
        percent1 = round(len(self.matching_pairs)/(self.g1.number_of_nodes()/100), 2)
        percent2 = round(len(self.matching_pairs)/(self.g2.number_of_nodes()/100), 2)
        print(f"Found a total of {len(self.matching_pairs)} "+
              f"matching nodes ({percent1}% of g1 & {percent2}% of g2)")
        return self.matching_pairs