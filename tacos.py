from helpers.time import time_passed
from helpers.parser import taco_parser
from helpers.banner import get_banner
from helpers.call_graph import generate_call_graph
from helpers.clique_algo_select import select_clique_algorithm
from helpers.modular_product_graph import get_modular_product_graph
from helpers.export import export_clique
from helpers.shared_graphs import generate_shared_graphs
from globals import set_args
from helpers.debug import debug_print
import networkx as nx
import os
import pickle
import time


if __name__ == "__main__":

    start_time = time.time()

    # Retrieve arguments
    args = taco_parser().parse_args()

    # Make taco
    print(get_banner(args.max_taco))

    # Process arguments
    args.gml_1 = args.gml_1.removesuffix(".gml")
    args.gml_2 = args.gml_2.removesuffix(".gml")
    args.gml_1 = args.gml_1.removesuffix(".apk")
    args.gml_2 = args.gml_2.removesuffix(".apk")
    if args.module_product_graph:
        args.module_product_graph = args.mpg.removesuffix(".pickle")
    if args.max_clique:
        args.max_clique = args.max_clique.removesuffix(".pickle")
    set_args(args)

    # Generate call graphs if --generate_call_graph is specified
    if args.generate_call_graph:
        generate_call_graph([args.gml_1, args.gml_2])
        debug_print(2, f"Generated call graphs in {time_passed(start_time)}")

    # Generate suffix for output file
    preprocess_time = time.time()
    sfx = f'-{args.algorithm}'
    if args.strip_leaves:
        sfx += '-leafless'
    if args.line_graph:
        sfx += '-line_based'

    # Read graph files
    debug_print(2,"Opening files")
    g1 = nx.read_gml(f"data/{args.gml_1}.gml")
    g2 = nx.read_gml(f"data/{args.gml_2}.gml")

    # Strip leaves from graphs if --strip_leaves is specified
    if args.strip_leaves:
        g1_leaves = [node for node in g1.nodes if g1.degree(node) == 1]
        g2_leaves = [node for node in g2.nodes if g2.degree(node) == 1]
        g1.remove_nodes_from(g1_leaves)
        g2.remove_nodes_from(g2_leaves)
    
    # Attempt to store pre-processed files in ./temp
    try:
        os.mkdir("temp")
    except:
        pass
    finally:
        nx.write_gml(g1, f"temp/{args.gml_1}{sfx}.gml")
        nx.write_gml(g2, f"temp/{args.gml_2}{sfx}.gml")

    # Make graphs unidirectional if not using SALSA
    if args.algorithm != 'salsa':    
        g1 = g1.to_undirected()
        g2 = g2.to_undirected()

    # Transform to line graph if --line_graph is specified
    if args.line_graph:
        g1 = nx.line_graph(g1)
        g2 = nx.line_graph(g2)
    debug_print(2, 
        f"Opened and pre-processed graphs in {time_passed(preprocess_time)}")

    # Create modular product graph if the clique-finding algorithm needs it
    if args.algorithm not in ('dp', 'rascal', 'salsa'):
        g3 = get_modular_product_graph(g1, g2, sfx)
    else:
        debug_print(1, f"Clique-finding algorithm {args.algorithm} does not "+
              "require a modular product graph. Skipping MPG generation.")
        g3 = None

    # Find the maximum clique if none is provided
    if not args.max_clique:
        debug_print(1,"Starting maximum clique-finding algorithm.")
        preclique_time = time.time()
        maximum_clique = select_clique_algorithm(g1, g2, g3)
        debug_print(2, f"Found maximum clique in {time_passed(preclique_time)}")

        # Write out maximum clique
        export_clique(sfx, maximum_clique)

    else:
        debug_print(1,
            "Using provided maximum clique. Skipping max clique generation.")
        with open(f"intermediates/{args.max_clique}.pickle", "rb") as f:
            if not args.line_graph:
                maximum_clique = pickle.load(f)

    # Stash g3 and reopen files for shared graph generation
    g3 = None
    g1 = nx.read_gml(f"temp/{args.gml_1}{sfx}.gml")
    g2 = nx.read_gml(f"temp/{args.gml_2}{sfx}.gml")
    try:
        os.remove(f"temp/{args.gml_1}{sfx}.gml")
        os.remove(f"temp/{args.gml_2}{sfx}.gml")
        os.rmdir("temp")
    except:
        pass

    # Generate the shared graphs
    shared_time = time.time()
    generate_shared_graphs(g1, g2, maximum_clique)
    
    # Write out generated graphs
    try:
        os.mkdir("output")
    except:
        pass
    finally:
        nx.write_gml(g1, f"output/{args.gml_1}{sfx}-shared.gml")
        nx.write_gml(g2, f"output/{args.gml_2}{sfx}-shared.gml")
    debug_print(2, f"Generated shared subgraph in {time_passed(shared_time)}")
    debug_print(2, f"Total runtime: {time_passed(start_time)}")

